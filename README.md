# My Configuration

This is just a collection of files that I install on my Macs that makes
my development life easier. A lot of it just makes the command line easier
to use with git.

This does not include my `nvim` config, which is in a different repo.

